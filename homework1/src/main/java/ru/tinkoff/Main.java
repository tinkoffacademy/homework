package ru.tinkoff;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

    public static class Main {
        private static final int MAX_ITERATION = 6;
        private static final int MAX_ITEM = 100;
        private static Object locker;
        private static int counter;
        private static int curNumber = 1;
        private static BlockingQueue<Integer> itemQueue;
        private static Object printLocker;


        public static void main(String[] args) {

            startTrade();
        }


        public static List<Integer> processNumber(List<Integer> numbers) {
            return numbers.stream().map(num -> (int) Math.pow(num, 2) + 10)
                    .filter(num -> ((num % 10 != 5) && (num % 10 != 6)))
                    .toList();
        }

        public static Map<Integer, Long> countNumber(List<Integer> numbers) {
            return numbers.stream().collect(Collectors.groupingBy(num -> num, Collectors.counting()))
                    .entrySet().stream().filter((k) -> (k.getValue() > 1))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }


        public static void printOne() {
            synchronized (locker) {
                while (counter < MAX_ITERATION) {
                    if (curNumber == 1) {
                        System.out.print(1);
                        curNumber = 2;
                        locker.notifyAll();
                    }
                    try {
                        locker.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                locker.notifyAll();
            }
        }


        public static void printTwo() {
            synchronized (locker) {
                while (counter < MAX_ITERATION) {

                    if (curNumber == 2) {
                        System.out.print(2);
                        curNumber = 3;
                        locker.notifyAll();
                    }
                    try {
                        locker.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                locker.notifyAll();
            }
        }


        public static void printThree() {
            synchronized (locker) {
                while (counter < MAX_ITERATION) {
                    if (curNumber == 3) {
                        System.out.print(3);
                        curNumber = 1;
                        counter++;
                        locker.notifyAll();
                    }
                    try {
                        locker.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                locker.notifyAll();
            }
        }


        public static void startThread() {

            counter = 0;
            locker = new Object();

            Thread thread1 = new Thread(ru.tinkoff.Main::printOne);
            Thread thread2 = new Thread(ru.tinkoff.Main::printTwo);
            Thread thread3 = new Thread(ru.tinkoff.Main::printThree);
            thread1.start();
            thread2.start();
            thread3.start();

            try {
                thread1.join();
                thread2.join();
                thread3.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


        public static void store() {
            int curItem = 0;
            Random randomGenerator = new Random();
            while (curItem < MAX_ITEM) {
                try {
                    itemQueue.put(curItem);
                    synchronized (printLocker) {
                        System.out.println("Add: " + curItem);
                    }
                    curItem++;
                    Thread.sleep(randomGenerator.nextInt(300));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }


        public static void consume() {
            int curItem = 0;
            Random randomGenerator = new Random();
            while (curItem < MAX_ITEM) {
                try {
                    int item = itemQueue.take();
                    synchronized (printLocker) {
                        System.out.println("Delete: " + item);
                    }
                    curItem++;
                    Thread.sleep(randomGenerator.nextInt(200));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }


        public static void startTrade() {
            itemQueue = new ArrayBlockingQueue<Integer>(10);
            Thread consumer = new Thread(ru.tinkoff.Main::consume);
            Thread store = new Thread(ru.tinkoff.Main::store);
            consumer.start();
            store.start();
            printLocker = new Object();
            try {
                store.join();
                consumer.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}