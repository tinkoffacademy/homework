package ru.tinkoff;


public class Board {

    private int size;
    private Symbol[][] field;
    private int paintedCellCounter;


    public Board(int size) {
        this.size = size;
        paintedCellCounter = 0;

        initField(size);
    }


    private void initField(int size) {
        field = new Symbol[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                field[i][j] = Symbol.N;
            }
        }
    }


    public void setSymbol(int row, int col, Symbol symbol) {
        field[row][col] = symbol;
        paintedCellCounter++;
    }


    public boolean isIncorrectPosition(int row, int col) {
        return (row < 0 || row >= size || col < 0 || col >= size);
    }


    public boolean checkWin(Symbol winSymbol) {
        if (checkRows(winSymbol))
            return true;

        if (checkColumns(winSymbol))
            return true;

        if (checkMainDiagonal(winSymbol))
            return true;

        return (checkSecondaryDiagonal(winSymbol));
    }


    public void print() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                System.out.print(field[i][j] + " ");
            System.out.println();
        }
    }

    private boolean checkRows(Symbol winSymbol) {
        boolean isWin;
        for (int i = 0; i < size; i++) {
            isWin = true;
            for (int j = 0; j < size; j++)
                if (!field[i][j].equals(winSymbol)) {
                    isWin = false;
                    break;
                }

            if (isWin)
                return true;
        }
        return false;
    }

    private boolean checkColumns(Symbol winSymbol) {
        boolean isWin;
        for (int i = 0; i < size; i++) {
            isWin = true;
            for (int j = 0; j < size; j++)
                if (!field[j][i].equals(winSymbol)) {
                    isWin = false;
                    break;
                }

            if (isWin)
                return true;
        }
        return false;
    }

    private boolean checkMainDiagonal(Symbol winSymbol) {
        boolean isWin = true;
        for (int i = 0; i < size; i++)
            if (!field[i][i].equals(winSymbol)) {
                isWin = false;
                break;
            }
        return isWin;
    }

    private boolean checkSecondaryDiagonal(Symbol winSymbol) {
        boolean isWin = true;
        for (int i = 0; i < size; i++)
            if (!field[i][size - i - 1].equals(winSymbol)) {
                isWin = false;
                break;
            }
        return isWin;
    }

    public Board copy() {
        Board boardCopy = new Board(size);

        for (int i = 0; i < size; i++) {
            System.arraycopy(field[i], 0, boardCopy.field[i], 0, size);
        }
        boardCopy.size = this.size;
        boardCopy.paintedCellCounter = this.paintedCellCounter;
        return boardCopy;
    }


    public boolean isEmptyField(Integer row, Integer col) {
        return field[row][col].equals(Symbol.N);
    }


    public int getSize() {
        return size;
    }


    public boolean isFullBoard() {
        return (paintedCellCounter == size * size);
    }
}

enum Symbol {
    X,
    O,
    N
}
