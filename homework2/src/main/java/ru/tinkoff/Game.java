package ru.tinkoff;

import java.util.Scanner;

public class Game {
    private Board board;
    private Player player;
    private MinMaxBot minMaxBot;
    private Scanner scanner;
    private Symbol playersSymbol;
    private Symbol botSymbol;

    public Game(Scanner scanner) {
        this.scanner = scanner;

        player = new Player(scanner);

        playersSymbol = Symbol.X;
        botSymbol = Symbol.O;

        minMaxBot = new MinMaxBot(botSymbol,playersSymbol);

    }

    private void  initBoard(){
        System.out.println("Введите размер поля");
        int boardSize = scanner.nextInt();
        board = new Board(boardSize);
        player.setBoardSize(boardSize);
    }

    public void start() {
        Step curStep;
        initBoard();

        while (true) {

            board.print();

            curStep = player.makeMove();

            while (! board.isEmptyField(curStep.x(), curStep.y())) {
                System.out.println("Ячейка занята");
                curStep = player.makeMove();
            }

            board.setSymbol(curStep.x(), curStep.y(), playersSymbol);

            if (isGameOver())
                break;

            board.print();

            curStep = minMaxBot.makeMove(board);

            board.setSymbol(curStep.x(), curStep.y(),botSymbol);

            if (isGameOver())
                break;

        }
    }

    private boolean isGameOver(){

        if (board.checkWin(playersSymbol)) {
            System.out.println("Player win");
            return true;
        }

        if (board.checkWin(botSymbol)) {
            System.out.println("Bot win");
            return true;
        }

        if (board.isFullBoard()) {
            System.out.println("Draw");
            return  true;
        }

        return false;
    }
}
