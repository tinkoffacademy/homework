package ru.tinkoff;

import java.util.function.BiPredicate;

public class MinMaxBot {

    private final Symbol botSymbol;
    private final Symbol playersSymbol;

    public MinMaxBot(Symbol botSymbol, Symbol playersSymbol) {
        this.botSymbol = botSymbol;
        this.playersSymbol = playersSymbol;
    }

    public Step makeMove(Board board) {
        return findNextMove(board, botSymbol).bestStep();
    }

    private Score findNextMove(Board board, Symbol curSymbol) {


        if (board.checkWin(botSymbol))
            return new Score(null, 10);

        if (board.checkWin(playersSymbol))
            return new Score(null, -10);

        if (board.isFullBoard())
            return new Score(null, 0);

        BiPredicate<Integer, Integer> lesserThan = (left, right) -> left < right;
        BiPredicate<Integer, Integer> greaterThan = (left, right) -> left > right;

        int curScore;
        Step curStep;


        Score bestMove = new Score(new Step(0, 0), Integer.MIN_VALUE);
        if (curSymbol == Symbol.X)
            bestMove = new Score(new Step(0, 0), Integer.MAX_VALUE);
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                if (board.isEmptyField(i, j)) {
                    Board boardCopy = board.copy();
                    boardCopy.setSymbol(i, j, curSymbol);
                    curStep = new Step(i, j);
                    curScore = findNextMove(boardCopy, invertSymbols(curSymbol)).bestScore();
                    if (curSymbol == botSymbol) {
                        bestMove = updateScore(greaterThan, new Score(curStep, curScore), bestMove);
                    } else {
                        bestMove = updateScore(lesserThan, new Score(curStep, curScore), bestMove);
                    }
                }
            }
        }
        return  bestMove;
    }


    private Score updateScore(BiPredicate<Integer, Integer> updatingRule, Score curScore, Score bestScore) {
        if (updatingRule.test(curScore.bestScore(), bestScore.bestScore())) {
            return curScore;
        }
        return bestScore;
    }

    private Symbol invertSymbols(Symbol curSymbol) {
        return curSymbol.equals(Symbol.X) ? Symbol.O : Symbol.X;
    }
}

record Score(Step bestStep, int bestScore) {
}

record Step(Integer x, Integer y){
}
