package ru.tinkoff;

import java.util.Scanner;

public class Player {

    private final Scanner scanner;
    private Integer boardSize;

    public Step makeMove() {
        int x, y;
        do{
        System.out.println("Input x coord");
        x = scanner.nextInt()-1;
        System.out.println("Input y coord");
        y = scanner.nextInt()-1;
        } while (isIncorrectPosition(x, y));

        return new Step(x, y);
    }

    public Player(Scanner scanner) {
        this.scanner = scanner;
    }

    public Integer getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(Integer boardSize) {
        this.boardSize = boardSize;
    }

    public boolean isIncorrectPosition(int row, int col) {
        return (row < 0 || row >= boardSize || col < 0 || col >= boardSize);
    }

}
